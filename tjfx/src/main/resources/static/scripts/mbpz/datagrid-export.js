/**
 * Created by WEI on 2017/07/03.
 * datagrid数据表格导出excel工具
 */

(function () {
    $.extend($.fn.datagrid.methods, {
        exportExcel: function (obj, param) {
            var url = param.url;
            var title = param.title;
            var sql = param.sql;
            var params = param.params;
            //http://127.0.0.1:8780/tjfx/datagridExport/easyui
            if (!url)
                throw new Error("url must not be null");
            if (!title)
                throw new Error("title must not be null");
            var form = $("<form>").attr({action: url, method: "post"});
            var setting = [];
            var frozenColumns = $(obj).datagrid("options").frozenColumns;
            var columns = $(obj).datagrid("options").columns;
            $.each(columns, function (i, row) {
                try{
                    setting.push([].concat(frozenColumns[i],columns[i]));
                }catch(err){
                    console.error(err)
                }
            });
            for(var i = 0; i < setting.length; i++) {
                for(var j = 0; j < setting[i].length; j++) {
                    if (setting[i][j] == undefined) {
                        setting[i].splice(j, 1);
                        j = j - 1;
                    }else{
                        var col = setting[i][j];
                        if(col.rowspan1){
                            col.rowspan = col.rowspan1;
                            delete col.rowspan1;
                        }
                    }
                }
            }
            var settingInp = $("<input>").attr({"name": "settingStr"}).val(JSON.stringify(setting));
            form.append(settingInp);
            var paramsInp = $("<input>").attr({"name": "params"}).val(params);
            form.append(paramsInp);
            var sqlInp = $("<input>").attr({"name": "sql"}).val(sql);
            form.append(sqlInp);
            var titleInp = $("<input>").attr({"name": "title"}).val(title);
            form.append(titleInp);
            $("body").append(form);
            form.submit();
            form.remove();
        }
    });
})();