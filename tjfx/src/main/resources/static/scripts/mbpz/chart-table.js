/**
 * 表格
 */
var chartTable = (function(chartTable)
{
	var version = $.QuickVersion;
	var scrollWidth = version['isWeb'] ? 23 : 3;
	var localOption = {
		bg_color : [ "yellow", "red", "blue" ],
		font_color : [ "white", "black" ],
		head_bg_color : [ "#ffffff" ],
		head_font_color : [ "#000000" ],
		rows : 0,// 每次查询新增数据量
		page : "false",// 是否分页
		current_row : 0,// 当前最大行
		pagesize : 0,// 每次加载数据量
		firstpagesize : 0,// 初始页数据量
		columns : [],
        frozenColumns : []
	};
	var instances = {};
	var loadData = function(_this)
	{
		var bodyContainer = $(_this.getDom()).find(".bodyContainer");
		var _bodyTable = bodyContainer.find("table");
		var _total = _bodyTable.find("tr").length;
		var _opt = _this.getOption();
		var current_row_length = _bodyTable.find("tr").length;
		for (var i = 0; i < _opt.rows; i++)
		{
			var bodyTr = $("<tr>");
			var colorSize = _opt.bg_color.length, fontColorSize = _opt.font_color.length;
			var current_index = _bodyTable.find("tr").length;
			colorSize = current_index % colorSize;
			fontColorSize = current_index % fontColorSize;
			for ( var j in _opt.columns)
			{
				var col = _opt.columns[j];
				var field = col.field, width = col.width;
				var value = col.data[i+current_row_length];
				var td = $("<td>").html(value).height("30px");
				td.css({
					background : _opt.bg_color[colorSize],
					color : _opt.font_color[fontColorSize],
					"text-align" : col.algin,
					padding : "10px"
				});
				bodyTr.append(td);
			}
			_bodyTable.append(bodyTr);
		}
	}
	chartTable.getInstanceByDom = function(dom)
	{
		return instances[$(dom).parents(".form-widget-control-wapper").attr("id")];
	}
	chartTable.init = function(dom, option, charItemName)
	{
		var instance = {};
		instance.itemName = charItemName || "pz";
		instances[$(dom).parents(".form-widget-control-wapper").attr("id")] = instance;
		var o = $.extend({}, localOption, option);
		$(dom).empty();
		var containerWidth = $(dom).width(), containerHeight = $(dom).height();
		// var widthTotal = 0;
		// 初始化头部表格
		var headerContainer = $("<div>").addClass("headerContainer").css({
			margin : "0",
			padding : "0",
			height : "30px",
			width : containerWidth + "px"
		});
		$(dom).append(headerContainer);
		var headerTable = $("<table>").css({
			margin : "0",
			padding : "0",
			background : o.head_bg_color[0],
			height : "30px",
			width : (containerWidth - scrollWidth) + "px"
		});
		headerContainer.append(headerTable);
		var headerTr = $("<tr>");
		for ( var i in o.columns)
		{
			var column = o.columns[i];
			var width = column.width;
			var td = $("<td>").html(column.title);
			td.css({
				color : o.head_font_color[0],
				"text-align" : column.algin
			});
			headerTr.append(td);
		}
		headerTable.append(headerTr);

		// 初始化内容表格
		var bodyContainer = $("<div>").addClass("bodyContainer").css({
			margin : "0",
			padding : "0",
			"overflow-y" : "scroll",
			"overflag-x" : "hidden",
			"overflowY" : "scroll",
			"overflagX" : "hidden",
			height : (containerHeight - 60) + "px",
			width : containerWidth + "px"
		});
		$(dom).append(bodyContainer);
		var bodyTable = $("<table>").css({
			margin : "0",
			padding : "0",
			width : (containerWidth - scrollWidth) + "px"
		});
		bodyContainer.append(bodyTable);
		instance.loadPage = function()
		{
			var __this = this;
			var _opt = __this.getOption();
			var _sql = _opt.sql;
			var _firstpagesize = parseInt(_opt.firstpagesize);
			var _pagesize = parseInt(_opt.pagesize);
			var _current_row = parseInt(_opt.current_row);
			var _row = parseInt(_opt.rows);
			var _page = _opt.page;
			// 判断是否需要查询
			if (_row < _pagesize)
			{
				return false;
			}
			if(this.itemName == "view")
			{
				var dom = this.getDom();
				var params = $(dom).data("params");
				var list = [{id : "id",
					sql : _sql,
					type : "table",
					page : _page,
					minnumber : _current_row,
					maxnumber : _current_row + _pagesize
				}]
				queryChartBySqlList(JSON.stringify(list), function(result)
						{
							try
							{
								result = JSON.parse(result[0].result);
								if (result.state == "true")
								{
									var rs_rows = result.rows;
									_opt.rows = rs_rows;
									_opt.current_row = _opt.current_row + rs_rows;
									for ( var i in _opt.columns)
									{
										var _col = _opt.columns[i];
										_col.data = _col.data.concat(result[_col.field]);
									}
									loadData(__this);
								} 
								else if (result.state == "false")
								{
									$.QuickAlert.alertFail({
										content : result.faultInfo
									});
								}
								else if (result.state == "kong")
								{
									_opt.page = "false";
									var bodyContainer = $(dom).find(".bodyContainer");
									var _bodyTable = bodyContainer.find("table");
									var bodyTr = $("<tr>");
									var td = $("<td>").html("数据加载完毕...").height("30px");
									$(dom).find(".bodyContainer").unbind('scroll');
									bodyTr.append(td);
									_bodyTable.append(bodyTr);
								}
							} catch (e)
							{
								console.error(e);
							}
						}, JSON.stringify(params));
			}
			else if(this.itemName == "pz")
			{
				queryChartBySql(_sql, "table", function(result)
						{
							try
							{
								if (result.state == "true")
								{
									var rs_rows = result.rows;
									_opt.rows = rs_rows;
									_opt.current_row = _opt.current_row + rs_rows;
									for ( var i in _opt.columns)
									{
										var _col = _opt.columns[i];
										_col.data.concat(result[_col.field]);
									}
									loadData(__this);
								} else
								{
									$.QuickAlert.alertFail({
										content : result.faultInfo
									});
								}
							} catch (e)
							{
								console.error(e);
							}
						}, {
							page : _page,
							minnumber : _current_row,
							maxnumber : _current_row + _pagesize
						});
			}
		}
		instance.setOption = function(option)
		{
			var dom = this.getDom();
			$(dom).empty();
			$.extend(this, chartTable.init(dom, option, this.itemName));
			instance.resize();
		}
		instance.getOption = function()
		{
			return o;
		}
		instance.getDom = function()
		{
			return dom;
		}
		instance.resize = function()
		{
			var dom = this.getDom();
			var option = this.getOption();
			var containerWidth = $(dom).width(), containerHeight = $(dom).height();
			var hc = $(dom).find(".headerContainer"),bc = $(dom).find(".bodyContainer");
			hc.width(containerWidth - scrollWidth);
			hc.find("table").width(containerWidth - scrollWidth);
			bc.width(containerWidth);
			bc.find("table").width(containerWidth - scrollWidth);
			bc.height(containerHeight - 60);
			var widthTotal = 0;
			for ( var i in option.columns)
			{
				var column = option.columns[i];
				widthTotal += parseInt(column.width) || 1;
			}
			widthTotal = (containerWidth - scrollWidth) / widthTotal;
			$(dom).find("tr").each(function()
			{
				for ( var i in option.columns)
				{
					$(this).find("td").eq(i).width(option.columns[i].width * widthTotal);
				}
			});
			return this;
		}
		$(dom).on("resize", instance.resize());
		$(dom).find(".bodyContainer").scroll(function()
		{
			var scrollTop = $(this).scrollTop();
			var maxTop = $(this).find("table").height();
			var parentHeight = $(this).parent().height() - 30;
			if (maxTop - parentHeight - scrollTop == -30 )
			{
				// 分页查询
				instance.loadPage();
			}
		});
		loadData(instance);
		instance.resize();
		return instance;
	}
	return chartTable;
})({});
