/**
 * 
 */
/** 选择器元数据父类 */
function metadata_condition(params)
{

	var metadata_condition_control = {};
	this.metadata = $.extend({}, metadata_condition_control, params);
	this.getMetadata = function()
	{
		return this.metadata;
	};
	this.setMetadata = function(metadataName, value)
	{
		this.metadata[metadataName] = value;
		$(this).trigger("metadata_change", {
			"metadataName" : metadataName,
			"value" : value
		});
	}
}

/** 选择器 控制面板 控制器 */
function metadata_view(control)
{
	this.control = control;
	this.linkageMetadata = function(metadataName, value)
	{
		this.control.setMetadata(metadataName, value);
	};
}

/** 元数据 选择器 视图 */
function metadata_form_view(control)
{
	this.control = control;
}
