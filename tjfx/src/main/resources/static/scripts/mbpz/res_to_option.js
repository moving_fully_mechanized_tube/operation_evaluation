/** 结果集解析放入option */
function resToOption() {
    this.getOption = function (result, opt) {
        var view_common = function (result) {
            var bottom = opt.grid[0].bottom || 1;
            var rotate = opt.xAxis[0].axisLabel.rotate || 0;
            var sin = Math.cos(rotate);
            sin = Math.abs(sin);
            var bs = parseInt(bottom)/25/sin;//倍数
            if (result.state == "true") {
                var defaultOpt = {
                    interval: 0,
                    formatter: function (value) {
                        var ret = value;// 拼接加\n返回的类目项
                        var maxLength = bs;// 每项显示文字个数
                        var valLength = (value + "").replace(/[\u0391-\uFFE5]/g,"aa").length;// X轴类目项的文字个数,真实长度计算：先把中文替换成两个字节的英文，再计算长度

                        if(valLength > maxLength){
                            ret = value.substring(0, maxLength) + "...";
                        }
                        return ret;
                    }
                };
                function getAxis(axis,data){
                    if(axis && axis.type != "value"){
                        axis.data = data.NAME;
                        if(axis.axisLabel){
                            axis.axisLabel.formatter = defaultOpt.formatter;
                        }
                    }
                }

                var option = opt;// $.extend(defaultOpt,opt);

                getAxis(option.xAxis[0], result);
                getAxis(option.xAxis[1], result);
                getAxis(option.yAxis[0], result);
                getAxis(option.yAxis[1], result);
                if(option.tooltip && option.tooltip.position){
                    delete option.tooltip.position;
                }

                var series = option.series;
                for (var i in series) {
                    series[i].data = result[series[i].column];
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }
        /** 散点图 */
        var view_scatter = function (result) {
            if (result.state == "true") {
                var defaultOpt = {
                    interval: 0,
                    formatter: function (value) {
                        var ret = "";// 拼接加\n返回的类目项
                        var maxLength = 5;// 每项显示文字个数
                        var valLength = value.length;// X轴类目项的文字个数
                        var rowN = Math.ceil(valLength / maxLength); // 类目项需要换行的行数
                        if (rowN > 1)// 如果类目项的文字大于5,
                        {
                            var temp = "";// 每次截取的字符串
                            var start = 0;// 开始截取的位置
                            var end = start + maxLength;// 结束截取的位置
                            temp = value.substring(start, end) + "...";
                            ret += temp; // 凭借最终的字符串
                            return ret;
                        } else {
                            return value;
                        }
                    }
                };

                var option = opt;// $.extend(defaultOpt,opt);
                /*
                 * option.tooltip= function(){ trigger: 'axis', axisPointer: {
                 * type: 'cross', animation: false, label: { backgroundColor:
                 * '#505765' } } };
                 */
                var hzb = option.xAxis[0].name;
                var zzb = option.yAxis[0].name;
                var series = option.series;
                var seriesOpt = [];
                var seriesMb = option.series[0];
                var data = result;
                var lengData = [];
                for (var key in data) {
                    if (key == "state" || key == "type") {
                        continue;
                    }
                    var seriesData = data[key];
                    var s = $.extend({}, seriesMb, {
                        data: seriesData,
                        name: key,
                        symbolSize: function (seriesData) {
                            return Math.sqrt(seriesData[2]) / 50;
                        },
                        label: {
                            emphasis: {
                                show: false,
                                position: 'top',
                                formatter: function (value) {
                                    var v = value.data[3];
                                    v += '\n';
                                    v += hzb + ":" + value.data[0];
                                    v += '\n';
                                    v += zzb + ":" + value.data[1];
                                    return v;
                                }
                            },
                            normal: {
                                show: false,
                                formatter: function (value) {
                                    var v = value.data[3];
                                    v += '\n';
                                    v += hzb + ":" + value.data[0];
                                    v += '\n';
                                    v += zzb + ":" + value.data[1];
                                    return v;
                                }
                            }
                        }
                    });

                    seriesOpt.push(s);
                    lengData.push(key);
                }
                var t = $.extend({}, option, {
                    tooltip: {
                        triggerOn: "click",
                        axisPointer: {
                            type: '',
                            animation: false,
                            label: {
                                backgroundColor: '#505765',
                            }
                        },
                        "formatter": function (p) {
                            var showType = "";
                            showType += p.seriesName;
                            showType += ":";
                            showType += p.data['3'];
                            showType += "<br>";
                            showType += hzb;
                            showType += ':';
                            showType += p.data['0'];
                            showType += "<br>";
                            showType += zzb;
                            showType += ':';
                            showType += p.data['1'];
                            return showType;
                        }
                        //formatter:"sdsadasdsa",
                    }
                });
                option = t;
                option.series = seriesOpt;
                option.legend.data = lengData;

                console.log(JSON.stringify(option));
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }
        /** 条形图 */
        var view_bar = function (result) {
            return view_common(result);
        }
        /** 柱状图 */
        var view_column = function (result) {
            return view_common(result);
        }
        /** 折线图 */
        var view_line = function (result) {
            return view_common(result);
        }
        /** 列表表格（类似top表） */
        var view_overviewtable = function (result) {
            return view_toptable(result);
        }
        /** 仪表盘 */
        var view_gauge = function (result) {
            if (result.state == "true") {
                var option = opt;
                var series = option.series;
                for (var i in series) {
                    var v = result[option.series[i].column];
                    option.series[i].data[0].value = v;
                    if (v > 100) {
                        var max = Math.ceil(v / 100) * 100;
                        option.series[i].max = max;
                    }
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }
        /** 环形图 */
        var view_pie = function (result) {
            if (result.state == "true") {
                var option = opt;
                var series = option.series;
                series[0].label = {
                    normal: {
                        show: true,
                        position: 'inner',
                        formatter: '{d}%'
                    }
                };
                option.legend.data = result.legend;
                for (var i in series) {
                    var val = result[series[i].column];
                    for (var j in val) {
                        if (StringUtils.isEmpty(val[j].value)) {
                            throw new Error("返回结果为空");
                        }
                    }
                    series[i].data = val;
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }
        /** 金字塔 */
        var view_pyramid = function (result) {
            if (result.state == "true") {
                var option = opt;
                var series = option.series;
                option.yAxis[0].data = result.NAME;
                if (option.yAxis[1]) {
                    option.yAxis[1].data = result.NAME;
                }
                option.xAxis[0].max = result.max;
                option.xAxis[0].min = 0 - parseInt(result.max);
                if (option.xAxis[1]) {
                    option.xAxis[1].max = result.max;
                    option.xAxis[1].min = 0 - parseInt(result.max);
                }
                for (var i in series) {
                    series[i].data = result[series[i].column];
                }
                option.tooltip.formatter = function (p) {
                    var str = "";
                    for (var i in p) {
                        var name = p[i].name;
                        var seriesName = p[i].seriesName;
                        var value = p[i].value;
                        value = value < 0 ? -value : value;
                        str += i == 0 ? name + '<br>' : '';
                        str += seriesName + ':' + value + '<br>';
                    }
                    return str;
                }
                option.xAxis[0].axisLabel.formatter = function (value, index) {
                    return Math.abs(value);
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }
        /** 雷达图 */
        var view_radar = function (result) {
            if (result.state == "true") {
                var option = opt;
                var series = option.series;
                option.radar.indicator = result.indicator;
                for (var i in series) {
                    var val = result[option.series[i].column];
                    if (StringUtils.isEmpty(val.join(""))) {
                        throw new Error("返回value为空");
                    } else {
                        option.series[i].data[i].value = val;
                    }
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }

        /** 漏斗图 */
        var view_funnel = function (result) {
            if (result.state == "true") {
                var option = opt;
                var series = option.series;
                option.legend.data = result.legend;
                for (var i in series) {
                    var val = result[series[i].column];
                    for (var j in val) {
                        if (StringUtils.isEmpty(val[j].value)) {
                            throw new Error("返回结果为空");
                        }
                    }
                    series[i].data = val;
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }

        /** 报表 */
        var view_normaltable = function (result) {
            if (result.state == "true") {
                var option = opt;
                option.rows = result.rows;
                option.current_row = parseInt(result.rows);
                for (var i in option.columns) {
                    var column = option.columns[i];
                    column.data = result[column.field];
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }

        /** top表格 */
        var view_toptable = function (result) {
            if (result.state == "true") {
                var option = opt;
                option.rows = result.rows;
                for (var i in option.columns) {
                    var column = option.columns[i];
                    column.data = result[column.field];
                }
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }
        }

        /**  多坐标系条形图  */
        var view_gridbar = function (result) {
            function setOptionXaxis(param) {
                var len = param.length;
                var xAxis = [];
                for (var i = 0; i < len; i++) {
                    var item = {
                        axisTick: {show: false},
                        axisLabel: {show: false},
                        splitLine: {show: false},
                        axisLine: {show: false},
                        nameGap: 0,
                        gridIndex: i,
                        name: param[i],
                        nameLocation: 'middle',
                        type: 'value'
                    };
                    xAxis.push(item);
                }
                return xAxis;
            }

            function setOptionYaxis(param) {
                var len = param.len, yAxisData = param.yAxisData;
                var yAxis = [];
                for (var i = 0; i < len; i++) {
                    var yd = i == 0 ? yAxisData : [];
                    var item = {
                        axisLine: {show: i == 0},
                        axisTick: {show: false},
                        axisLabel: {show: true},
                        nameTextStyle: {},
                        gridIndex: i,
                        boundaryGap: true,
                        type: 'category',
                        name: "",
                        data: yd
                    };
                    yAxis.push(item);
                }
                return yAxis;
            }

            function setOptionSeries(param) {
                var series = [];
                var len = param.length;
                for (var i = 0; i < len; i++) {
                    var d = param[i];
                    var dataList = d.list, name = d.name;
                    var item = {
                        name: name,
                        xAxisIndex: i,
                        yAxisIndex: i,
                        column: "SUMVALUE1",
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        type: 'bar',
                        symbolSize: 8,
                        hoverAnimation: false,
                        data: dataList,
                    };
                    series.push(item);
                }
                return series;
            }

            function setOptionGrid(param) {
                var len = param.len, g = param.grid;
                var top = parseFloat(g.top);
                var bottom = parseFloat(g.bottom);
                var left = parseFloat(g.left);
                var right = parseFloat(g.right);
                var totalWidth = 100 - (left + right);
                var eachWidth = totalWidth / len;
                var grids = [];
                for (var i = 0; i < len; i++) {
                    var item = {};
                    item.top = top;
                    item.bottom = bottom;
                    item.left = (left + eachWidth * i) + '%';
                    item.width = (eachWidth - 2) + '%';
                    grids.push(item);
                }
                return grids;
            }

            if (result.funcName) {
                var params = result.params;
                var funcName = result.funcName;
                var res = ""
                eval("res = " + funcName + "(params)");
                return res;
            }
            if (result.state == "true") {
                var option = opt;
                var grid = option.GridSettings;
                var xAxisData = result.legend;
                var yAxisData = result.yAxis;
                var seriesData = result.series;
                var legendData = result.legend;
                var len = seriesData.length;
                option.grid = setOptionGrid({len: len, grid: grid});
                option.series = setOptionSeries(seriesData);
                option.xAxis = setOptionXaxis(xAxisData);
                option.yAxis = setOptionYaxis({len: len, yAxisData: yAxisData});
                option.legend.data = legendData;
                return option;
            } else if (result.state == "false") {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            } else if (result.state == "kong") {
                throw new Error("返回结果为空");
            }


        }
        /**  多坐标轴折线图  */
        var view_axisLine = function (result) {
            if (result.state == "true") {
                var option = opt;
                option.tooltip.formatter = "{a} <br/>{b}: {c}" + result.DW;
                option.xAxis[0].data = result.NAME;
                option.xAxis[1].data = result.NAME1;
                var legendBarData = [], legendLineData = [];
                for (var i in option.series) {
                    option.series[i].data = result[option.series[i].column];
                    if (option.series[i].type == "line") {
                        legendLineData.push({
                            name: option.series[i].name,
                            icon: "line"
                        });
                    } else {
                        legendBarData.push({
                            name: option.series[i].name,
                            icon: "roundRect"
                        });
                    }
                }
                legendBarData.push("");
                option.legend.data = $.merge(legendBarData, legendLineData);
                return option;
            }
            else {
                console.error(result.faultInfo);
                throw new Error(result.faultInfo);
            }
        }

        var type = result["type"];
        var func_name = "view_" + type;
        func_name = eval(func_name);
        return func_name.apply(null, [result]);
    }
    return this;
}
