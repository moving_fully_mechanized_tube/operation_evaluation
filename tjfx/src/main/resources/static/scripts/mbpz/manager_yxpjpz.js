$(function () {
    receiveCondition();
});

function receiveCondition() {
    var BDID = $.QuickUrlUtils.getRequest("BDID");
    if (!StringUtils.isEmpty(BDID)) {
        var url = "/tjfx/yxpjpz/receiveYxpjpz";
        $.QuickRemote.AjaxJson(url, receiveConditionHandler, {
            BDID: BDID
        }, {
            BDID: BDID
        });
    }
}

/** 获取模板配置回调 */
function receiveConditionHandler(result, BDID) {
    var formMetadata = result["formMetadata"];
    formMetadata = JSON.parse(formMetadata);
    var isLinkResource = formMetadata.isLinkResource || "false";
    formMetadata.isLinkResource = isLinkResource;
    setFormMetadate(JSON.stringify(formMetadata), BDID);
    delete result["formMetadata"];
    $("#form-content").formContent("setFormConfig", {
        config: result
    });
}

function saveHandler() {
    /** 控制面板校验 */
    var flag = $(".form-widget-sortable-wapper-active").triggerHandler("focusout");
    if (flag === undefined) {
        $.QuickAlert.alertFail({
            content: "必须选择组件!"
        });
        return false;
    } else if (flag === false) {
        return false;
    }

    /** 控件唯一性校验:时间控件有且只有一个，下拉框控件ID不重复 */
    var formMetadate = $("#form-content").formContent("getFormConfig");
    var dateCount = 0;
    var idList = [];
    for (var key in formMetadate) {
        var data = formMetadate[key];
        if (data.type == "date") {
            dateCount++;
        } else if (data.type != "date") {
            var id = data.CONTROL_ID;
            if (!id)continue;
            if (id == "startDate" || id == "endDate") {
                $.QuickAlert.alertFail({
                    content: "选择条件不能与时间控件重名!"
                });
                flag = false;
                return false;
            }
            for (var i in idList) {
                if (id == idList[i]) {
                    $.QuickAlert.alertFail({
                        content: "选择条件ID=[" + id + "]重复!"
                    });
                    flag = false;
                    return false;
                }
            }
            idList.push(id);
        }
        if (!flag)
            return false;
    }
    /** 校验表单设置 */
    flag = $("#form-setting").find(".quick-field").Validater();
    if (flag === true)
        saveHandlerAfterValidate();
}

function saveHandlerAfterValidate() {
    var data = {};

    var BDID = $.QuickUrlUtils.getRequest("BDID");
    data.BDID = BDID || "";

    /** 原始html */
    data["content"] = encodeURIComponent($("#form-content").formContent("getContent"));

    /** 配置的数据 */
    var contentData = encodeURIComponent(JSON.stringify($("#form-content").formContent("getFormConfig")));
    data["contentData"] = contentData;
    /** 表单元数据 */
    var formMetadate = getFormMetadate();
    /**  是否是下钻页面  */
    data["isLinkResource"] = formMetadate.isLinkResource;
    formMetadate = encodeURIComponent(JSON.stringify(formMetadate));
    data["formMetadata"] = formMetadate;

    /** 格式化html */
    var btn = "";
    btn += "<script type='text/javascript'>";
    btn += "var contentData = \"" + contentData + "\";";
    btn += "var formMetadate = \"" + formMetadate + "\";";
    btn += "var userInfo = '#userInfo#';";
    btn += "</script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/jquery/jquery-3.1.1.min.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/echarts/echarts.min.3.6.2.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.parser.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.linkbutton.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.panel.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.resizable.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.pagination.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.datagrid.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/easyui/1.5.1/plugins/jquery.treegrid.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/layui/2.3.0/layui.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/layui/2.3.0/lay/modules/layer.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/jqueryui/1.12.1/jquery-ui.min.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/StringUtils.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-base.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-parser.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-remote.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-version.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-view.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-field.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-alert.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-validate.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/quick-utils.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/StoreCache.js'></script>";
    btn += "<script type='text/javascript' src='/quickJs/scripts/common/Date.js'></script>";
    btn += "<link type='text/css' rel='stylesheet' href='/quickJs/scripts/easyui/1.5.1/themes/bootstrap/easyui.css'/>";
    btn += "<link type='text/css' rel='stylesheet' href='/quickJs/scripts/easyui/1.5.1/themes/icon.css' />";
    btn += "<link type='text/css' rel='stylesheet' href='/quickJs/scripts/layui/2.3.0/css/layui.css' />";
    btn += "<link type='text/css' rel='stylesheet' href='/quickJs/scripts/jqueryui/1.12.1/jquery-ui.min.css' />";

    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/yxpj_parse.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/chart-table.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/chart-table-top.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/res_to_option.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/control-sql.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/zbmx.js'></script>";
    btn += "<script type='text/javascript' src='/tjfx/scripts/mbpz/datagrid-export.js'></script>";

    btn += "<script type='text/javascript' src='/tjfx/scripts/bootstrap-select/js/bootstrap-select.js'></script>";
    btn += "<link type='text/css' rel='stylesheet' href='/tjfx/scripts/bootstrap-select/css/bootstrap-select.css' />";
    btn += "<link type='text/css' rel='stylesheet' href='/tjfx/scripts/bootstrap/3.0.0/css/bootstrap.min.css' />";
    btn += "<script type='text/javascript' src='/tjfx/scripts/bootstrap/3.0.0/js/bootstrap.min.js'></script>";
    btn += "<link type='text/css' rel='stylesheet' href='/tjfx/styles/view/yxpjfx.css' />";

    data["formatContent"] = encodeURI($("#form-content").formContent("getFormatContent") + btn);

    var url = "/tjfx/yxpjpz/saveYxpjpz";
    $.QuickRemote.AjaxJson(url, saveConditionHandler, data, formMetadate);
}

function saveConditionHandler(result, formMetadate) {
    if (result.state == "true") {
        setFormMetadate(JSON.stringify(formMetadate), {
            BDID: result.BDID
        });
        $.QuickAlert.alertNormal("保存成功", {
            callback: function () {
                close();
            }
        });
    }
}