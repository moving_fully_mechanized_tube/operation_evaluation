/**
 * 表单拖拽
 */
/** 拖拽排序公用配置 */
var sortableConfig = {
	items : ".form-widget-sortable-wapper",
	placeholder : "predefined-widget-item-placeholder",
	opacity : 0.5,
	connectWith : ".form-widget-sortable",
	receive : function(evt, ui)
	{
		var target = ui["helper"];
		if (target && target.hasClass("predefined-widget-item"))
		{
			$(target).formWidgetSortableWapper("init");
			$(target).trigger("click");
		}
	},
    update:function(vet, ui){
		ui.item.children(".chart-box").trigger("resize");
	}
};

$(function()
{
	initView();
})

function initView()
{
	$(".predefined-widget-item").draggable({
		connectToSortable : ".widget-sortable",
		helper : "clone",
		opacity : 0.5,
		zIndex : 100,
		appendTo : "body"
	}).disableSelection();

	$("#form-content").sortable(sortableConfig).disableSelection();

	initSettingPanel();

	$("#form-content").formContent();

	$("#form-control-setting").formControlSetting();
	$("#form-layout-setting").formLayoutSetting();
};

/** 获取表单配置元数据 */
function getFormMetadate()
{
	var data = $("#form-content").data("formMetadata");
	if (!data)
		data = {};
	$("#form-setting").find(".quick-field").each(function()
	{
		var name = $(this).attr("name");
		var value = $(this).QuickField("getData");
		data[name] = value;
	})
	return data;
}

/** 设置表单配置元数据 */
function setFormMetadate(formMetadata, BDID)
{
	formMetadata = JSON.parse(formMetadata);
	$.extend(formMetadata, BDID);
	$("#form-content").data("formMetadata", formMetadata);
	$("#form-setting").find(".quick-field").each(function()
	{
		var name = $(this).attr("name");
		var value = formMetadata[name];
		$(this).QuickField("setData", value);
	})
}

/** 返回按钮回调 */
function backClickHandler()
{
	close();
	// var ref = '';
	// if (document.referrer.length > 0)
	// {
	// 	ref = document.referrer;
	// }
	// try
	// {
	// 	if (ref.length == 0 && opener.location.href.length > 0)
	// 	{
	// 		ref = opener.location.href;
	// 	}
	// } catch (e)
	// {
	// }
    //
	// location = ref;
}

/** 初始化设置面板 */
function initSettingPanel()
{
	/** tab切换回调处理 */
	$("#settingTabs a[data-toggle='tab']").on("show.bs.tab", function()
	{
	})
	/** 表格操作按钮 */
	$(".table-setting-btn").on("click", function()
	{
		var operator = $(this).attr("operator");
		var widgetWapper = $(".form-widget-sortable-wapper-active").formWidgetSortableWapper("getFormWidgetWapper");
		if (widgetWapper)
		{
			var widget = $(widgetWapper).children().get(0);
			if ($(widget).prop("localName") == "table")
			{
				$(widget).formWidgetLayoutTable(operator, 1);
			} else
			{
				$.QuickAlert.alertFail({
					content : "请选中表格"
				});
			}
		}
	})
};

/** 拖动排序组件 */
function FormWidgetSortableWapper(jq, params)
{
	/**
	 * param:{focus:"获取焦点的对象",callback:"回调"}
	 */
	$(jq).on("focusout", function(event, param)
	{
		param = param || {};
		// 如果点击的是同一个对象，则返回false，
		if (event.currentTarget == param.focus)
			return false;
		//判断对象类型
		var type = $(this).find(".form-widget-layout-wapper").attr("type");
		if(type=="swiper"){//如果是滑动布局，则判断是否含有空白页
			var isEmptyPage = true;
			$(this).find(".swiper-slide").each(function(i,item){
				var child = $(item).find(".form-widget-sortable-wapper");
				if(!child){
					isEmptyPage = false;
					return false;
				}
			});
			if(!isEmptyPage){
				return false;
			}
		}
		var controlWapper = $(this).children().eq(0).data("FormWidgetControlWapper");
		// 如果不是图表组件，则不校验，返回undefined
		if (controlWapper == undefined)
			return undefined;
		// 如果不存在校验，则返回true
		var validateFun = controlWapper.validate();
		if (!$.isFunction(validateFun))
			return true;
		// 如果存在校验，优先进行校验
		var validate = validateFun();
		// 校验不通过，触发this的click
		if (validate != true)
			$(this).trigger("click");
		// 返回校验结果：考虑校验方法返回值有异常，所以只有校验结果为true时返回true
		return validate == true ? true : false;
	});
	$(jq).on("click", function(e)
	{
		var flag = $(".form-widget-sortable-wapper-active").triggerHandler("focusout", {
			focus : this
		});
		if (flag != false)// 增加第一个组件时，没有选中的对象，返回值为undefined
		{
			$("#form-content").find(".form-widget-sortable-wapper").removeClass("form-widget-sortable-wapper-active");
			/** 当前对象激活显示 */
			$(this).addClass("form-widget-sortable-wapper-active");
			$("#form-content").find(".widget-delete-btn").hide();
			$(this).children(".widget-delete-btn").show();

			$($(this).children().get(0)).trigger("select-setting");
		}
	});

	$(jq).on("click", ".widget-delete-btn", function()
	{
		var _this = $(this);
		$.QuickAlert.alertConfirm({
			content : "是否删除",
			callback : function()
			{
				$(_this).parent().remove();
				$("#form-layout-setting").trigger("showSelectLayoutTip");
				$("#form-control-setting").trigger("showSelectControlTip");
			}
		});
		return false;
	});

	function addDeletBtn()
	{
		/** 添加删除按钮 */
		var delBtn = $($("#widget-delete-btn").html());
		var widgetType = $(jq).attr("widgetType");
		if (widgetType.indexOf("form-widget-control") != -1)
		{
			$(delBtn).addClass("widget-delete-btn-control");
		} else if (widgetType.indexOf("form-widget-layout") != -1)
		{
			$(delBtn).addClass("widget-delete-btn-layout");
		}
		$(delBtn).appendTo($(jq));
	}

	this.init = function(params)
	{
		jq.empty();
		jq.removeClass();
		jq.attr("style", "");
		jq.addClass("form-widget-sortable-wapper");
		/** 初始化内部组件 */
		var widgetType = $(jq).attr("widgetType");
		var type = widgetType.substring(widgetType.lastIndexOf("-") + 1, widgetType.length);
		var componentHtml = $("#" + widgetType).html();
		var child = $(componentHtml);
		$(child).appendTo($(jq));

		var childId = new Date().getTime();
		if (widgetType.indexOf("form-widget-control") != -1)
		{
			params = $.extend(params, {
				"type" : type
			})
			childId = "control_wapper_" + childId;
			$(child).attr("id", childId);
			$(child).formWidgetControlWapper(params);
		} else if (widgetType.indexOf("form-widget-layout") != -1)
		{
			params = $.extend(params, {
				"type" : type
			})
			childId = "layout_wapper_" + childId;
			$(child).attr("id", childId);
			$(child).formWidgetLayoutWapper(params);
		}
		$(child).attr("type", type);
		addDeletBtn();
	}

	/** 获取实际表单元素的wapper */
	this.getFormWidgetWapper = function()
	{
		return $(jq).children().get(0);
	}
};

$.fn.formWidgetSortableWapper = function(options, params)
{
	var obj = $(this).data("FormWidgetSortableWapper");
	if ($.isEmptyObject(obj))
	{
		obj = new FormWidgetSortableWapper(this, options);
		$(this).data("FormWidgetSortableWapper", obj);
	}
	if ($.type(options) == "string")
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
};

/** 表单布局控件包装层 */
function FormWidgetLayoutWapper(jq, params)
{
	var child = $(jq).children("[layout-type]");
	var type = $(child).attr("layout-type");


	/** 视图对象 */
	var layoutView = eval("new formWidgetLayout_" + type + "(child, params)");

	/** 联动控制面板 */
	$(jq).on("select-setting", function()
	{
		/** 设置对应的控制面板显示 */
		$("#settingTabs a[href='#form-layout-setting']").tab("show");
		$("#form-layout-setting").formLayoutSetting("removeAll");
		layoutView.setSettingPanel($("#form-layout-setting"));
		return false;
	});
};

$.fn.formWidgetLayoutWapper = function(options, params)
{
	var obj = $(this).data("FormWidgetLayoutWapper");
	if ($.isEmptyObject(obj))
	{
		obj = new FormWidgetLayoutWapper(this, options);
		$(this).data("FormWidgetLayoutWapper", obj);
	}
	if ($.type(options) == "string")
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
}

/** 表单控件包装层 */
function FormWidgetControlWapper(jq, params)
{
	/** 控件类型 */
	var type = params["type"];
	/** 元数据 数据控制器 */
	var metadataControl = new MetadataControlFactory(type, params);

	/** 元数据 控制面板视图控制器 */
	var metadataSettingView = new MetadataViewSettingFactory(type, metadataControl);

	/** 元数据 表单视图控制器 */
	var metadataFormView = MetadataFormViewFactory(type, jq, metadataControl);

	/** 获取元数据 */
	this.getMetadata = function()
	{
		return metadataControl.getMetadata();
	}

	this.validate = function()
	{
		return metadataSettingView.validate;
	}

	$(metadataControl).on("metadata_change", function(evt, data)
	{
		var metadataName = data["metadataName"];
		var func = metadataFormView[metadataName];
		if ($.isFunction(func))
		{
			metadataFormView[metadataName](data["value"]);
		}
	})

	/** 联动控制面板 */
	$(jq).on("select-setting", function(event)
	{
		$("#settingTabs a[href='#form-control-setting']").tab("show");
		/** 设置控制面板 */
		$("#form-control-setting").formControlSetting("removeAll");
		$.each(metadataControl.getMetadata(), function(key)
		{
			var func = metadataSettingView[key];
			if ($.isFunction(func))
			{
				metadataSettingView[key]();
			}
		});
        event.stopPropagation();
		return false;
	});

	var metadata = this.getMetadata();
	$.each(metadata, function(metadataName, value)
	{
		var func = metadataFormView[metadataName];
		if ($.isFunction(func))
		{
			metadataFormView[metadataName](value);
		}
	});
};
$.fn.formWidgetControlWapper = function(options, params)
{
	var obj = $(this).data("FormWidgetControlWapper");
	if ($.isEmptyObject(obj))
	{
		if ($.isPlainObject(options))
		{
			obj = new FormWidgetControlWapper(this, options);
		} else
		{
			obj = new FormWidgetControlWapper(this);
		}
		$(this).data("FormWidgetControlWapper", obj);
	}
	if ($.type(options) == "string")
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
};

/** ****************************************************元数据 数据控制器工厂 */
function MetadataControlFactory(type, params)
{
	return eval("new metadata_" + type + "(params)");
}
/** 元数据属性设置 控制面板视图 工厂 */
function MetadataViewSettingFactory(type, control)
{
	return eval("new metadata_view_" + type + "(control)");
}
/**
 * ******************控件元数据表单视图控制器
 * 属性值得改变直接反应到视图上****************************************
 */

function MetadataFormViewFactory(type, control, metadataControl)
{
	return eval("new metadata_form_view_" + type + "(control, metadataControl)");
}

/** 输入控件设置面板类 form-control-setting* */
function FormControlSetting(jq, params)
{
	var controlWapper = null;

	/** 自定义事件 提示选择一个控件 */
	$(jq).on("showSelectControlTip", function()
	{
		$(jq).children(".form-setting-group").addClass("hidden");
		$(jq).children(".control-tip-select").removeClass("hidden");
	})

	this.removeAll = function()
	{
		$(jq).children(".control-tip-select").addClass("hidden");
		$(jq).children(".form-setting-group").remove();
	}
};

$.fn.formControlSetting = function(options, params)
{
	var obj = $(this).data("FormControlSetting");
	if ($.isEmptyObject(obj))
	{
		obj = new FormControlSetting(this);
		$(this).data("FormControlSetting", obj);
	}
	if ($.type(options) == "string" && obj[options])
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
}

/** 布局 控制面板 */
function FormLayoutSetting(jq, params)
{
	/** 初始化隐藏所有控件 */
	$(jq).children(".form-setting-group").hide();

	/** 自定义事件 提示选择一个控件 */
	$(jq).on("showSelectLayoutTip", function()
	{
		$(jq).children(".form-setting-group").hide();
		$(jq).children(".layout-tip-select").show();
	})

	this.setValue = function(params)
	{
		var type = params["type"];
		$(jq).children(".form-setting-group").show();
		$(jq).children("div:not(." + type + ")").hide();
	}

	this.removeAll = function()
	{
		$(jq).children(".layout-tip-select").addClass("hidden");
		$(jq).children(".form-setting-group").remove();
	}
}
$.fn.formLayoutSetting = function(options, params)
{
	var obj = $(this).data("FormLayoutSetting");
	if ($.isEmptyObject(obj))
	{
		obj = new FormLayoutSetting(this);
		$(this).data("FormLayoutSetting", obj);
	}
	if ($.type(options) == "string" && obj[options])
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
};

/** 表单 */
function FormContent(jq, params)
{
	/** 获取原始的内容 */
	this.getContent = function()
	{
		$(jq).find(".form-widget-sortable-wapper-active").removeClass("form-widget-sortable-wapper-active");
		/** swiper处理 */
		$(jq).find(".swiper-slide-active").removeClass("swiper-slide-active");
		$(jq).find(".swiper-wrapper").each(function()
		{
			$(this).attr("style", "");
			$(this).children(".swiper-slide").each(function()
			{
				$(this).removeClass("swiper-slide-prev");
				$(this).removeClass("swiper-slide-active");
				$(this).removeClass("swiper-slide-next");
			})
		});
		/**  datagrid处理，销毁实例，每次初始化重新创建   */
		$(jq).find(".echarts-div .datagrid-container").each(function(i,item){
            var container = $(item).prop("outerHTML");
            $(item).parents(".echarts-div").empty().append(container);
		});
		var content = $(jq).prop("outerHTML");
		var background = new RegExp("background\\s*\\:\\s*url\\((\\\")http\:\/\/\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\:\\d{1,5}\/[a-zA-Z0-9\\/_]*(\\\")\\)");
		if (background.test(content))
		{
			var backgroundImg = content.match(background);
			var bImg = backgroundImg[0].replace("\"", "'");
			bImg = bImg.substring(0, bImg.length - 2);
			bImg = bImg + "')";
			content = content.replace(backgroundImg[0], bImg);
		}

		return content;
	};
	/** 获取格式化之后的内容 */
	this.getFormatContent = function()
	{
		return format();
	}
	/** 预览 */
	this.preview = function()
	{
		var contentId = new Date().getTime();
		StoreCache.setCache(contentId, format());
		StoreCache.setCache(contentId + "_data", innerGetMetadata());
		window.open($.QuickUrlUtils.getProjectName() + "/views/form/form_preview.jsp?contentId=" + contentId);
	}
	/** 获取表单的所有配置 */
	this.getFormConfig = function()
	{
		return innerGetMetadata();
	}
	/** 设置表单配置 */
	this.setFormConfig = function(metadata)
	{
		if (metadata)
		{
			var config = metadata["config"];
			var content = config["content"];
			if (content)
			{
				$(content).children().appendTo($(jq));
			}

			$(jq).find(".form-widget-sortable-wapper").each(function()
			{
				$(this).formWidgetSortableWapper();
			});

			/** 初始化所有的布局控件 */
			$(jq).find(".form-widget-layout-wapper").each(function()
			{
				$(this).formWidgetLayoutWapper({
					parseHTML : true
				});
			});

			/** 表单控件的配置元数据 */
			var contentData = JSON.parse(config["contentData"]);
			if (contentData)
			{
				$.each(contentData, function(key, value)
				{
					$("#" + key).formWidgetControlWapper(value);
				})
			}
		}
	}

	/** 获取控件的配置数据 */
	function innerGetMetadata()
	{
		/** 所有控件的值 */
		var controlsData = {};
		$(jq).find(".form-widget-control-wapper").each(function()
		{
			var data = $(this).formWidgetControlWapper("getMetadata");
			var id = $(this).attr("id");
			controlsData[id] = data;
		})
		return controlsData;
	}

	/** 格式化为正式内容 */
	function format()
	{
		var content = $(jq).clone();
		$(content).find(".form-widget-sortable-wapper").each(function()
		{
			var parent = $(this).parent().get(0);
			$(this).children().appendTo($(parent));
			$(this).remove();
		})
		$(content).find(".swiper-slide-active").removeClass("swiper-slide-active");
		/** 删除布局控件多余的的元素 */
		$(content).find(".form-widget-layout-wapper").each(function()
		{
			var child = $(this).find("div:eq(0)");
			$(this).empty();
			$(this).append(child);
		});
		/**  删除图表的画布 */
        content.find(".echarts-div").empty();
		/**  删除表格布局控件多余的元素  */
		$(content).find("table.from-layout-table>tbody>tr>td>span").remove();
		/**  删除tabs标签页布局多余的元素  */
		$(content).find("div[layout-type=tabs]").removeClass("ui-tabs ui-corner-all ui-widget ui-widget-content");
        $(content).find("div[layout-type=tabs]").children("ul").removeAttr("class").removeAttr("role");
        $(content).find("div[layout-type=tabs]").children("ul").children("li").removeAttr("class").removeAttr("role").removeAttr("tabindex").removeAttr("aria-controls").removeAttr("aria-labelledby").removeAttr("aria-selected").removeAttr("aria-expanded");
        $(content).find("div[layout-type=tabs]").children("ul").children("li").children("a").removeAttr("role").removeAttr("tabindex").removeAttr("class").removeAttr("id");
        $(content).find("div[layout-type=tabs]").children("div").removeAttr("class").removeAttr("aria-labelledby").removeAttr("role").removeAttr("aria-hidden").removeAttr("style");

		/** 删除所有控制按钮 */
		$(content).find(".widget-delete-btn").remove();
		/** 删除所有排序功能和样式 */
		$(content).find(".ui-sortable").removeClass("form-widget-sortable-wapper").removeClass("ui-sortable");
		$(content).find(".form-widget-sortable-wapper-active").removeClass("form-widget-sortable-wapper-active");
		$(content).find(".form-widget-sortable").removeClass("form-widget-sortable");
		/** 删除所有table的head */
		$(content).find("table thead").remove();
		$(content).find("table .area").removeClass("area");
		$(content).find("tr").each(function()
		{
			var isEmpty = true;
			$(this).children().each(function()
			{
				if ($(this).children().length != 0)
				{
					isEmpty = false;
					return false;
				}
			})
			if (isEmpty)
			{
				$(this).remove();
			}
		});

		/** 解锁控件 */
		$(content).find("input").removeClass("css-disable");
		$(content).find("select").removeAttr("disabled");

		return $(content).prop("outerHTML");
	}
};
$.fn.formContent = function(options, params)
{
	var obj = $(this).data("FormContent");
	if ($.isEmptyObject(obj))
	{
		obj = new FormContent(this, options);
		$(this).data("FormContent", obj);
	}
	if ($.type(options) == "string" && obj[options])
	{
		return obj[options](params);
	}
	return this.each(function()
	{
	});
}
