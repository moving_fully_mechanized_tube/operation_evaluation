/**
 * 查询sql
 */
function queryBySql(sql, callback, params)
{
	if (sql)
	{
		params = $.extend({}, {
			sql : sql
		}, params);
		$.QuickRemote.AjaxJson("/tjfx/sqlExecuter/queryBySql", callback, params);
	}
}

/**
 * 查询sql，根据type返回不同格式结果
 */
function queryChartBySql(sql, type, callback, params, additionParams)
{
	if (sql && type)
	{
		params = $.extend({}, {
			sql : encodeURIComponent(sql)
		}, params);
		$.QuickRemote.AjaxJson("/tjfx/sqlExecuter/queryCharDataBySql/" + type, callback, params, additionParams);
	}
}

/**
 * 查询所有图表sql，并返回解析的值
 */
function queryChartBySqlList(list, callback, params)
{
	var listParams = {"list": list, "params": params};
	$.QuickRemote.AjaxJson("/tjfx/sqlExecuter/queryBySqlCharList", callback, listParams);
}

/**
 * 查询datagrid分页的url
 */
function queryDataGridByPage()
{
	return "/tjfx/sqlExecuter/queryDataGridByPage";
}
/**
 * 查询treegrid分页的url
 */
function queryTreeGridByPage()
{
    return "/tjfx/sqlExecuter/queryTreeGridByPage";
}
