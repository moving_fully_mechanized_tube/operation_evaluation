/**
 * TOP表格
 */

var chartTableTop = (function (chartTableTop) {
    var scrollWidth = 23;
    var instances = {};
    var localOption = function () {
        return {
            columns: [{
                field: ""
            }, {
                field: ""
            }, {
                field: ""
            }],
            data: [],
            showNumber: true,
            showDw: false,
            rows: 0
        };
    };
    chartTableTop.getInstanceByDom = function (dom) {
        return instances[$(dom).parents(".form-widget-control-wapper").attr("id")];
    }
    chartTableTop.init = function (dom, option) {
        var instance = {};
        instances[$(dom).parents(".form-widget-control-wapper").attr("id")] = instance;
        var o = localOption();
        o = $.extend(o, option);
        var ul = $("<ul>").addClass("search-data-list");
        for (var i = 0; i < o.rows; i++) {
            var li = $("<li>").addClass("list");
            var name = $("<span>").addClass("span-text").html(o.columns[0].data[i]);
            var value = $("<span>").addClass("span-number").html(o.columns[1].data[i]);

            if (o.showNumber) {
                var num = $("<i>").addClass("number").html((i + 1));
                li.append(num)
            }
            if (o.showDw) {
                var dw = $("<span>").addClass("span-dw").html(o.columns[2].data[i]);
                li.append(dw)
            }
            li.append(name).append(value);
            ul.append(li);
        }
        $(dom).append(ul);

        instance.setOption = function (option) {
            var dom = this.getDom();
            $(dom).empty();
            chartTableTop.init(dom, option);
        }
        instance.getOption = function () {
            return o;
        }
        instance.getDom = function () {
            return dom;
        }
        instance.resize = function () {
        }

        return instance;
    }
    return chartTableTop;
})({});