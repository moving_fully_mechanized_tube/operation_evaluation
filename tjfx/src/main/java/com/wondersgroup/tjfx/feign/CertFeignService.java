package com.wondersgroup.tjfx.feign;

import com.quick.framework.boot.web.feign.CertService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by lion on 2017/6/16.
 */
@FeignClient("${cert.CertServiceId}")
public interface CertFeignService extends CertService {
}
