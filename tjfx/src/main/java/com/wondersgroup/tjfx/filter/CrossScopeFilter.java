package com.wondersgroup.tjfx.filter;

import javax.servlet.*;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CrossScopeFilter implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	public void doFilter(ServletRequest servletRequest, ServletResponse response1,
						 FilterChain chain) throws IOException, ServletException {
		/** 跨域请求 */
		HttpServletResponse response = (HttpServletResponse)response1;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST,GET");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
		chain.doFilter(servletRequest, response);
	}

	@Override
	public void destroy() {
		
	}

}
