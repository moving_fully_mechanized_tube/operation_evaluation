package com.wondersgroup.tjfx.common.resultHandler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * 单指标专用ResultHandler，处理value，保留2位小数
 * 
 * @author WEI
 * 
 */
@Component("treegrid")
public class TreegridSqlResultHandler implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONArray jsonAry = new JSONArray();
		ResultSetMetaData rm = rs.getMetaData();
		while (rs.next()) {
			JSONObject json = new JSONObject();
			for (int i = 0; i < rm.getColumnCount(); i++) {
				String columnName = rm.getColumnLabel(i + 1).toUpperCase();
				String value = rs.getString(i + 1);
				json.put(columnName, value);
			}
			jsonAry.add(json);
		}
		JSONObject rows = new JSONObject();
		rows.put("total",jsonAry.size());
		rows.put("rows",jsonAry);
		ro.put("data", rows);
		return ro;
	}
}
