package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.druid.stat.TableStat.Name;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 单指标专用ResultHandler，处理value，保留2位小数
 * 
 * @author WEI
 * 
 */
@Component("datagrid")
public class DatagridSqlResultHandler implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONArray jsonAry = new JSONArray();
		ResultSetMetaData rm = rs.getMetaData();
		while (rs.next()) {
			JSONObject json = new JSONObject();
			for (int i = 0; i < rm.getColumnCount(); i++) {
				String columnName = rm.getColumnLabel(i + 1).toUpperCase();
				String value = rs.getString(i + 1);
				json.put(columnName, value);
			}
			jsonAry.add(json);
		}
		ro.put("data", jsonAry);
		return ro;
	}
}
