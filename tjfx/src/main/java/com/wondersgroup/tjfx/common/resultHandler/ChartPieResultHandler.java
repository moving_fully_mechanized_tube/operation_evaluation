package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Component("pie")
public class ChartPieResultHandler implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		List<String> columnNameList = new ArrayList<String>();
		DecimalFormat df = new DecimalFormat("#.###");
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONObject json = new JSONObject();
		ResultSetMetaData rm = null;
		rm = rs.getMetaData();
		JSONArray nameArr = new JSONArray();
		int length = 0;
		while (rs.next()) {
			length++;
			String name = rs.getString("NAME");
			nameArr.add(name);
			for (int i = 0; i < rm.getColumnCount(); i++) {
				/* 取字段名 */
				String columnName = rm.getColumnLabel(i + 1);
				columnName = columnName.toUpperCase();
				columnNameList.add(columnName);

				// 旧版本配置有id和key列，新版本不需要，忽略
				if ("ID".equals(columnName) || "KEY".equals(columnName))
					continue;

				/* 取值 */
				String value = rs.getString(i + 1);
				value = value == null ? "" : value;
				if (json.containsKey(columnName)) {
					JSONArray ar = json.getJSONArray(columnName);
					JSONObject j = new JSONObject();
					j.put("name", name);
					j.put("value", value);
					ar.add(j);
				} else {
					JSONArray ar = new JSONArray();
					JSONObject j = new JSONObject();
					j.put("name", name);
					j.put("value", value);
					ar.add(j);
					json.put(columnName, ar);
				}
			}
		}
		json.remove("NAME");
		json.put("legend", nameArr);
		if (length == 0) {
			ro.put("state", "kong");
		}
		columnNameList = removeDuplicate(columnNameList);
		columnNameList.remove("NAME");
		String key = columnNameList.get(0);
		String value = json.getString(key);
		JSONArray jsonAry = JSONArray.parseArray(value);
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < jsonAry.size(); i++) {
			JSONObject jsonObj = jsonAry.getJSONObject(i);
			list.add(jsonObj.getString("value"));
		}
		String cs;
		try {
			cs = average(list.toString());
		} catch (Exception e) {
			throw new SQLException("查询结果应为数值的列得到其他字符." + e.getLocalizedMessage());
		}
		String dw = getDw(cs);
		ro.put("DW", dw);
		JSONArray jary = new JSONArray();
		for (int i = 0; i < jsonAry.size(); i++) {
			JSONObject jsonObj = jsonAry.getJSONObject(i);
			JSONObject jsonOb = (JSONObject) jsonObj.clone();
			double val = Double.parseDouble(jsonObj.getString("value")) / Double.parseDouble(cs);
			jsonOb.put("value", df.format(val));
			jary.add(jsonOb);
		}
		JSONObject jo = new JSONObject();
		jo.put(key, jary);
		jo.put("legend", nameArr);
		ro.putAll(jo);
		return ro;
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 删除重复key
	 * 
	 * @param list
	 * @return
	 */
	public List<String> removeDuplicate(List<String> list) {
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = list.size() - 1; j > i; j--) {
				if (list.get(j).equals(list.get(i))) {
					list.remove(j);
				}
			}
		}
		return list;
	}

	/**
	 * 除数
	 * 
	 * @param str
	 * @return
	 */
	public static String average(String str) {
		String[] array = str.substring(1, str.length() - 1).replaceAll("\"", "").split(",");
		String result = "1";
		double temp = 0;
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + Double.parseDouble(array[i]);
		}
		temp = sum / array.length;
		if (temp > 10000) {
			result = "10000";
		} else if (temp > 100000000) {
			result = "100000000";
		}
		return result;
	}

	/**
	 * 获得单位
	 * 
	 * @param jh
	 * @return
	 */
	private String getDw(String jh) {
		String result = "";
		if ("10000".equals(jh)) {
			result = "万";
		} else if ("100000000".equals(jh)) {
			result = "亿";
		}
		return result;
	}

}
