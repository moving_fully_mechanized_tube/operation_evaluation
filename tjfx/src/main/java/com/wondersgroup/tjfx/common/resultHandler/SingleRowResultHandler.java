package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

/**
 * 单行结果集处理
 * @author Lion
 *
 */
@Component("singleRowResultHandler")
public class SingleRowResultHandler implements ResultSetExtractor<Object>{
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		JSONObject json = new JSONObject();
		ResultSetMetaData rm = null;
		rm = rs.getMetaData();
		if(rs.next())
		{
			for (int i = 0; i < rm.getColumnCount(); i++) 
			{
				/*取字段名*/
				String columnName = rm.getColumnLabel(i + 1);
				/*取值*/
				String value = rs.getString(i + 1) + "";
				json.put(columnName.toUpperCase(), value);
			}
		}
		return json;
	}
}
