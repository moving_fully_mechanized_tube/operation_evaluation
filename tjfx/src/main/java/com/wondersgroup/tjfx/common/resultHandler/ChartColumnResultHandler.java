package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Component("column")
public class ChartColumnResultHandler implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		List<String> columnNameList = new ArrayList<String>();
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONObject json = new JSONObject();
		ResultSetMetaData rm = null;
		rm = rs.getMetaData();
		int length = 0;
		while (rs.next()) {
			length++;
			for (int i = 0; i < rm.getColumnCount(); i++) {
				/* 取字段名 */
				String columnName = rm.getColumnLabel(i + 1);
				columnName = columnName.toUpperCase();
				columnNameList.add(columnName);

				// 旧版本配置有id和key列，新版本不需要，忽略
				if ("ID".equals(columnName) || "KEY".equals(columnName))
					continue;

				/* 取值 */
				String value = rs.getString(i + 1);
				value = value == null ? "" : value;
				if (json.containsKey(columnName)) {
					JSONArray ar = json.getJSONArray(columnName);
					ar.add(value);
				} else {
					JSONArray ar = new JSONArray();
					ar.add(value);
					json.put(columnName, ar);
				}
			}
			ro.putAll(json);
		}
		if (length == 0) {
			ro.put("state", "kong");
		}
		columnNameList = removeDuplicate(columnNameList);
		columnNameList.remove("NAME");
		String cs = "";
		try {
			for(int i=0;i<columnNameList.size();i++){
				String key = columnNameList.get(i);
				String value = ro.getString(key);
				cs = average(value);
				ro.put(key, conversion(cs,value));
			}
		} catch (Exception e) {
			throw new SQLException("查询结果应为数值的列得到其他字符." + e.getLocalizedMessage());
		}
		String dw = getDw(cs);
		ro.put("DW", dw);
		return ro;
	}
	
	/**
	 * 删除重复key
	 * @param list
	 * @return
	 */
	public List<String> removeDuplicate(List<String> list)   { 
	   for(int i = 0 ;i<list.size()-1;i++)   { 
	    for(int j = list.size()-1 ;j>i;j--)   { 
	      if(list.get(j).equals(list.get(i)))   { 
	        list.remove(j); 
	      } 
	    } 
	  } 
	  return list; 
	} 
	
	/**
	 * 除数
	 * @param str
	 * @return
	 */
	public static String average(String str) {
		String[] array = str.substring(1, str.length()-1).replaceAll("\"", "").split(",");
		String result = "1";
		double temp = 0;
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + Double.parseDouble(array[i]);
		}
		temp = sum / array.length;
		if(temp>10000){
			result = "10000";
		}else if(temp>100000000){
			result = "100000000";
		}
		return result;
	}
	
	/**
	 * 获得转换单位后的数组
	 * @param jh
	 * @param str
	 * @return
	 */
	private JSONArray conversion(String jh,String str){
		JSONArray ar = new JSONArray();
		DecimalFormat df = new DecimalFormat("#.###");
		String[] value = str.substring(1, str.length()-1).replaceAll("\"", "").split(",");
		for(int i=0;i<value.length;i++){
			String temp = value[i];
			double val = Double.parseDouble(temp)/Double.parseDouble(jh);
			ar.add(df.format(val));
		}
		return ar;
	}
	
	/**
	 * 获得单位
	 * @param jh
	 * @return
	 */
	private String getDw(String jh){
		String result = "";
		if("10000".equals(jh)){
			result = "万";
		}else if("100000000".equals(jh)){
			result = "亿";
		}
		return result;
	}

}
