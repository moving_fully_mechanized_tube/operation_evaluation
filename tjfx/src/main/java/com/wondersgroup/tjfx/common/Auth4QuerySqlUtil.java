package com.wondersgroup.tjfx.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * Created by WEI on 2017/07/21.
 * 为SQL查询增加相关的用户信息参数，控制数据权限
 */
public class Auth4QuerySqlUtil {

    private static final String USERINFO_XZQH = "USERINFO_XZQH";
    private static final String USERINFO_YLJG = "USERINFO_YLJG";

    /**
     * 根据用户类型，分别加上行政区划和医疗机构
     */
    public static String addUserinfo(String params, JSONObject userinfo) {
        JSONObject p = JSON.parseObject(params);
        String jglb = userinfo.getString("WSJGLBDM");
        String xzqh = userinfo.getString("JGXZQHDM");
        String jgdm = userinfo.getString("YLJGDM");
        if (Auth4QuerySqlUtil.isGljg(jglb)) {
            p.put(USERINFO_XZQH, xzqh);
        } else if (Auth4QuerySqlUtil.isYljg(jglb)) {
            p.put(USERINFO_YLJG, jgdm);
        }
        return p.toJSONString();
    }

    /**
     * 匹配登录用户的机构类别，是否是管理机构，机构类别代码是R开头的是管理机构，其他的是医疗机构
     *
     * @param jglb 机构类别代码
     * @return
     */
    private static boolean isGljg(String jglb) {
        String reg = "^R.*";
        if (StringUtils.isEmpty(jglb)) {
            return false;
        }
        return Pattern.matches(reg, jglb);
    }

    /**
     * 匹配登录用户的机构类别，是否是医疗机构，机构类别代码是R开头的是管理机构，其他的是医疗机构
     *
     * @param jglb 机构类别代码
     * @return
     */
    private static boolean isYljg(String jglb) {
        String reg = "^R.*";
        if (StringUtils.isEmpty(jglb)) {
            return false;
        }
        return !Pattern.matches(reg, jglb);
    }

}
