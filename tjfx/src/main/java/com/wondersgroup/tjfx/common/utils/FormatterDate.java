package com.wondersgroup.tjfx.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FormatterDate {

	/**
	 * 获取各维度的时间
	 * 
	 * @return
	 */
	public static Map<String, String> getFormatterDate() {
		Map<String, String> map = new HashMap<String, String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-01");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-01-01");

		Date currentTime = new Date();
		Calendar d = Calendar.getInstance();
		d.setTime(currentTime);
		currentTime = d.getTime();

		Calendar c = Calendar.getInstance();
		c.setTime(currentTime);
		Date current_date = c.getTime();// 当前日
		Date current_season = getFirstDayOfSeason(c);// 当前季度第一天
		c.setTime(currentTime);
		c.set(Calendar.DATE, c.get(Calendar.DATE) - 1);
		Date last_date = c.getTime();// 上一天
		c.setTime(currentTime);
		c.set(Calendar.DATE, 1);
		c.add(Calendar.DATE, -1);
		Date last_month = c.getTime();// 上一月最后一天
		c.setTime(currentTime);
		c.add(Calendar.MONTH, -3);
		Date last_season = getFirstDayOfSeason(c);// 上一季第一天
		c.setTime(currentTime);
		c.setTime(current_season);
		c.add(Calendar.DATE, -1);
		Date last_seasion_last_day = c.getTime();// 上一季最后一天
		c.setTime(currentTime);
		c.set(Calendar.DATE, 1);
		c.set(Calendar.MONTH, 0);
		c.add(Calendar.DATE, -1);
		Date last_year = c.getTime();// 上一年最后一天

		String current_startDay = sdf.format(current_date);// 当前日开始时间
		String current_endDay = sdf.format(current_date);// 当前日结束时间
		String current_startMonth = sdf2.format(current_date);// 当前月开始时间
		String current_endMonth = sdf.format(current_date);// 当前月结束时间
		String current_startSeason = sdf2.format(current_season);// 当前季开始时间
		String current_endSeason = sdf.format(current_date);// 当前季度结束时间
		String current_startYear = sdf3.format(current_date);// 当前年开始时间
		String current_endYear = sdf.format(current_date);// 当前年结束时间

		String last_startDay = sdf.format(last_date);// 上一日开始时间
		String last_endDay = sdf.format(last_date);// 上一日结束时间
		String last_startMonth = sdf2.format(last_month);// 上一月开始时间
		String last_endMonth = sdf.format(last_month);// 上一月结束时间
		String last_startSeason = sdf2.format(last_season);// 上一季开始时间
		String last_endSeason = sdf.format(last_seasion_last_day);// 上一季结束时间
		String last_startYear = sdf3.format(last_year);// 上一年开始时间
		String last_endYear = sdf.format(last_year);// 上一年结束时间

		map.put("current_startDay", current_startDay);
		map.put("current_endDay", current_endDay);
		map.put("current_startMonth", current_startMonth);
		map.put("current_endMonth", current_endMonth);
		map.put("current_startSeason", current_startSeason);
		map.put("current_endSeason", current_endSeason);
		map.put("current_startYear", current_startYear);
		map.put("current_endYear", current_endYear);

		map.put("last_startDay", last_startDay);
		map.put("last_endDay", last_endDay);
		map.put("last_startMonth", last_startMonth);
		map.put("last_endMonth", last_endMonth);
		map.put("last_startSeason", last_startSeason);
		map.put("last_endSeason", last_endSeason);
		map.put("last_startYear", last_startYear);
		map.put("last_endYear", last_endYear);

		return map;
	}
	
	/**
	 * 获取当前时间所在季度的第一天
	 * 
	 * @param c
	 * @return
	 */
	private static Date getFirstDayOfSeason(Calendar c) {
		int month = c.get(Calendar.MONTH);
		switch (month) {
		case 0:
		case 1:
		case 2:
			c.set(Calendar.MONTH, 0);
			break;
		case 3:
		case 4:
		case 5:
			c.set(Calendar.MONTH, 3);
			break;
		case 6:
		case 7:
		case 8:
			c.set(Calendar.MONTH, 6);
			break;
		case 9:
		case 10:
		case 11:
			c.set(Calendar.MONTH, 9);
			break;
		default:
			break;
		}
		c.set(Calendar.DATE, 1);
		return c.getTime();
	}
	
}
