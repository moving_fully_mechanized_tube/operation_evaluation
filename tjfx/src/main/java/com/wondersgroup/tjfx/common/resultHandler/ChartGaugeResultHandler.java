package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Component("gauge")
public class ChartGaugeResultHandler implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONObject json = new JSONObject();
		ResultSetMetaData rm = null;
		rm = rs.getMetaData();
		int length = 0;
		while (rs.next()) {
			length++;
			for (int i = 0; i < rm.getColumnCount(); i++) {
				/* 取字段名 */
				String columnName = rm.getColumnLabel(i + 1);
				columnName = columnName.toUpperCase();

				// 旧版本配置有id和key列，新版本不需要，忽略
				if ("ID".equals(columnName) || "KEY".equals(columnName))
					continue;

				/* 取值 */
				String value = rs.getString(i + 1);
				json.put(columnName, value);
			}
		}
		ro.putAll(json);
		if (length == 0) {
			ro.put("state", "kong");
		}
		return ro;
	}
}
