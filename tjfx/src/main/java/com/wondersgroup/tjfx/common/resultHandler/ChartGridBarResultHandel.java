package com.wondersgroup.tjfx.common.resultHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Component("gridbar")
public class ChartGridBarResultHandel implements ResultSetExtractor<JSONObject> {

	@Override
	public JSONObject extractData(ResultSet rs) throws SQLException, DataAccessException {
		JSONObject ro = new JSONObject();
		ro.put("state", "true");
		JSONArray ar = new JSONArray();
		JSONArray names = new JSONArray();
		JSONArray legend = new JSONArray();
		ResultSetMetaData rm = null;
		rm = rs.getMetaData();
		int columnCount = rm.getColumnCount();
		int length = 0;
		while (rs.next()) {
			length++;
			JSONObject json = new JSONObject();
			JSONArray valueAry = new JSONArray();
			String key = "";
			for (int i = 0; i < columnCount; i++) {
				String columnName = rm.getColumnLabel(i + 1);
				columnName = columnName.toUpperCase();
				/* 取值 */
				String value = rs.getString(i + 1);
				value = value == null ? "" : value;
				if ("NAME".equals(columnName)) {
					key = value;
					legend.add(value);
				} else {
					valueAry.add(value);
					if (length == 1)
						names.add(columnName);
				}
			}
			json.put("name", key);
			json.put("list", valueAry);
			ar.add(json);

		}
		ro.put("yAxis", names);
		ro.put("series", ar);
		ro.put("legend", legend);
		if (length == 0) {
			ro.put("state", "kong");
		} else {
		}

		return ro;
	}
}
