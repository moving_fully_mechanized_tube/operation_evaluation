package com.wondersgroup.tjfx.common.utils;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class XMLUtils {
	
	/**XML前缀*/
	public static String XML_PREFIX = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	public static String map2XMLCondition(Map<String, String> map)
	{
		Document doc = DocumentHelper.createDocument();
		Element root = doc.addElement("ROOT");
		 for (Object obj : map.keySet()) {  
	            Element keyElement = root.addElement((String)obj);  
	            keyElement.setText(String.valueOf(map.get(obj)));  
	        }  
		 return doc2String(doc);  
	}
	
	
	public static String doc2String(Document document) {  
        String s = "";  
        try {  
            // 使用输出流来进行转化   
            ByteArrayOutputStream out = new ByteArrayOutputStream();  
            // 使用UTF-8编码   
            OutputFormat format = new OutputFormat("   ", true, "UTF-8");  
            XMLWriter writer = new XMLWriter(out, format);  
            writer.write(document);  
            s = out.toString("UTF-8");
            s = s.replace(XML_PREFIX, "");
            s = s.replace("\n", "");
            s = s.replace(" ", "");
        } catch (Exception ex) {  
            ex.printStackTrace();  
       }  
        return s;  
    }  
}
