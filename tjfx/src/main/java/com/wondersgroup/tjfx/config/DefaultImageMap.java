package com.wondersgroup.tjfx.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@ConfigurationProperties(prefix = "form.defaultImage")
public class DefaultImageMap extends HashMap<String, String> {
	private static final long serialVersionUID = 1L;
}
