package com.wondersgroup.tjfx.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.alibaba.fastjson.util.TypeUtils;
import com.quick.framework.boot.pidLog.PIDListener;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;


@Configuration
@ImportResource(locations={"classpath:config/spring-application.xml"})
public class ApplicationConfig {

    /**
     * 替换默认的jackson做消息转换
     *
     * @return
     */
//    @Bean
//    public HttpMessageConverters fastJsonHttpMessageConverters() {
//        TypeUtils.compatibleWithJavaBean = true;
//        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat,
//                SerializerFeature.SkipTransientField,
//                SerializerFeature.WriteNullStringAsEmpty,
//                SerializerFeature.WriteMapNullValue);
//        fastConverter.setFastJsonConfig(fastJsonConfig);
//        /*添加支持的http contentType类型 不够 或者调用报错 可以继续添加*/
//        List<MediaType> list = new ArrayList<MediaType>(2);
//        list.add(MediaType.APPLICATION_JSON_UTF8);
//        list.add(MediaType.APPLICATION_JSON);
//        list.add(MediaType.APPLICATION_FORM_URLENCODED);
//        fastConverter.setSupportedMediaTypes(list);
//        return new HttpMessageConverters(fastConverter);
//    }

//    @Configuration
//    public class WebMvcConfig extends WebMvcConfigurerAdapter {
//
//        /**
//         * 利用fastjson替换掉jackson，且解决中文乱码问题
//         * @param converters
//         */
//        @Override
//        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//            FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
//            FastJsonConfig fastJsonConfig = new FastJsonConfig();
//            fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
//            //处理中文乱码问题
//            List<MediaType> fastMediaTypes = new ArrayList<>();
//            fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
//            fastConverter.setSupportedMediaTypes(fastMediaTypes);
//            fastConverter.setFastJsonConfig(fastJsonConfig);
//            converters.add(fastConverter);
//        }
//    }

    @Bean
    public PIDListener getPIDListener(){
        return new PIDListener();
    }
}
