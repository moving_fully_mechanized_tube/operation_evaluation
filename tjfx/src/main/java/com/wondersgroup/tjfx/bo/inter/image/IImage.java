package com.wondersgroup.tjfx.bo.inter.image;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public interface IImage {

	/**
	 * 查询图片资源
	 * @param request
	 * @return
	 */
	public String queryImage();
	
	/**
	 * 增加图片
	 * @param request
	 * @return
	 */
	public String addImage(Map<String, Object> map);
	/**
	 * 批量增加图片
	 * @param request
	 * @return
	 */
	public String addImage(List<Map<String, Object>> list);
	
	/**
	 * 删除图片资源
	 * @param request
	 * @return
	 */
	public String deleteImage(String id);
	
	/**
	 * 获取图片
	 * @param resid
	 * @param os
	 */
	public void retriveImg(String resid, OutputStream os) throws Exception;
	
	/**
	 * 获取缩略图
	 * @param resid
	 * @param os
	 */
	public void retrivePreImg(String resid, OutputStream os, int width, int height) throws Exception;
	
	public String getImageByte(String resid);
}
