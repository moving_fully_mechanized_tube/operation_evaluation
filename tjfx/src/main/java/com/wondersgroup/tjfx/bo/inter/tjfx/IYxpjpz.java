package com.wondersgroup.tjfx.bo.inter.tjfx;

import java.util.Map;

/**
 * Created by WEI on 2017/07/11.
 */
public interface IYxpjpz {
    String queryYxpjpz(Map<String, String> map);
    String receiveYxpjpz(String BDID);
    String receiveYxpjView(String BDID);
    String saveYxpjpz(Map<String, String> map);
    String deleteYxpjpz(String BDID);
    String queryFormList(String name, String isLink);
}
