package com.wondersgroup.tjfx.bo.impl.tjfx;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wondersgroup.tjfx.bo.inter.form.IForm;
import com.wondersgroup.tjfx.bo.inter.tjfx.IYxpjpz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * Created by WEI on 2017/07/11.
 */
@Service
public class YxpjpzImpl implements IYxpjpz {
    @Autowired
    IForm formDataService;
    private static final String TABLE_NAME = "yxpjpz";

    @Override
    public String queryYxpjpz(Map<String, String> map) {
        String formMetadata = map.get("formMetadata");
        if (formMetadata == null) formMetadata = "";
        return formDataService.queryForm(formMetadata, TABLE_NAME);

    }

    @Override
    public String receiveYxpjpz(String BDID) {
        return formDataService.receiveForm(TABLE_NAME, BDID);
    }

    @Override
    public String receiveYxpjView(String BDID) {
        return formDataService.receiveForm(TABLE_NAME, BDID);
    }

    @Override
    public String saveYxpjpz(Map<String, String> map) {
        String jsonStr = JSON.toJSONString(map);
        JSONObject json = JSONObject.parseObject(jsonStr);
        String BDID = map.get("BDID");
        if (StringUtils.isEmpty(BDID)) BDID = "";
        return formDataService.saveForm(BDID, json.toString(), TABLE_NAME);
    }

    @Override
    public String deleteYxpjpz(String BDID) {
        return formDataService.deleteForm(BDID, TABLE_NAME);
    }

    @Override
    public String queryFormList(String name, String isLink) {
        name = StringUtils.isEmpty(name) ? "" : name;
        String result = formDataService.queryLinked(TABLE_NAME, name, isLink);
        JSONArray ary = JSON.parseArray(result);
        return ary.toJSONString();
    }
}
