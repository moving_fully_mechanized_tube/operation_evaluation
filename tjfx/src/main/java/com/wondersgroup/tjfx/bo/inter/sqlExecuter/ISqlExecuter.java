package com.wondersgroup.tjfx.bo.inter.sqlExecuter;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public interface ISqlExecuter {
	/**
	 * 执行传入的sql，返回结果
	 * 
	 * @param sql
	 * @param map
	 * @return
	 */
	String queryBySql(String sql, Map<String, String> map);

	/**
	 * 根据图表类型，返回对应option结构的结果集
	 * 
	 * @param sql
	 * @param type
	 * @return
	 */
	String queryBySql(String sql, String type);

	/**
	 * 格式化sql，替换参数，根据图表类型，返回对应option结构的结果集
	 *
	 * @param sql
	 * @param type
	 * @param map
	 * @return
	 * @throws Exception
	 */
	String queryBySql(String sql, String type, Map<String, String> map) throws Exception;

	/**
	 *  格式化分页sql，替换参数，根据图表类型，返回对应option结构的结果集
	 *
	 * @param sql
	 * @param type
	 * @param map
	 * @return
	 * @throws Exception
	 */
	String queryByPageSql(String sql, String type, Map<String, String> map) throws Exception;

	/**
	 * 格式化分页sql，替换参数，返回Datagrid分页结果集
	 * @param sql
	 * @param map
	 * @return
	 * @throws Exception
	 */
	JSONObject queryByDatagridSql(String sql, Map<String, String> map) throws Exception;

	/**
	 * 格式化分页sql，替换参数，返回Datagrid分页结果集
	 * @param sql
	 * @param map
	 * @return
	 * @throws Exception
	 */
	JSONObject queryByTreegridSql(String sql, Map<String, String> map) throws Exception;

}
