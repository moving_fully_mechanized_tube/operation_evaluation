package com.wondersgroup.tjfx.bo.inter.form;

/**
 * 条件配置
 *
 * @author lion
 */
public interface IForm {

    /**
     * 保存
     *
     * @param map
     * @return
     */
    public String saveForm(String BDID, String jsonStr, String type);

    /**
     * 查询模板列表
     *
     * @param map
     * @return
     */
    public String queryForm(String formMetadata, String type);

    /**
     * 查询模版详细信息
     *
     * @param map
     * @return
     */
    public String queryFormXx(String type);

    /**
     * 获取模板配置
     *
     * @param map
     * @return
     */
    public String receiveForm(String formId, String type);

    /**
     * 删除模板配置
     *
     * @param map
     * @return
     */
    public String deleteForm(String formId, String type);

    /**
     * 根据类型获取resource_iframe表的数据
     *
     * @param type
     * @return
     */
    public String receiveIframe(String type);

    /**
     * 根据表名获取内容
     *
     * @param tableName
     * @return
     */
    public String receiveForm(String tableName);

    /**
     * 查询mongo中已配置视图的指标代码
     *
     * @param type
     * @return
     */
    public String queryZbst(String type);

    /**
     * 查询页面
     * @param type 表名
     * @param name 关键字
     * @param isLink 是否下钻页面
     * @return
     */
    String queryLinked(String type, String name, String isLink);
}
