package com.wondersgroup.tjfx.bo.impl.sqlExecuter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.quick.framework.boot.repository.sql.bean.storedProcedure.SPSQLBean;
import com.wondersgroup.tjfx.bo.inter.sqlExecuter.ICharDataHandler;
import com.wondersgroup.tjfx.bo.inter.sqlExecuter.ISqlExecuter;
import com.wondersgroup.tjfx.common.utils.FormatterDate;
import com.wondersgroup.tjfx.common.utils.XMLUtils;
import com.wondersgroup.tjfx.dao.inter.YdzgDataSpBeanDao;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.springframework.cache.annotation.Cacheable;

@Service("charDataHandler")
public class CharDataHanlderImpl implements ICharDataHandler {

    @Autowired
    private ISqlExecuter sqlExecuter;
    @Autowired
    YdzgDataSpBeanDao ydzgDataSpBeanDao;
    @Autowired
    ICharDataHandler charDataHandler;
    @Autowired
    ResultSetExtractor<JSONArray> dzbSqlResultHandler;

    /**
     * 批量查询图表数据并格式化
     *
     * @param map
     * @return
     */
//	@Cacheable(value = "queryBySqlCharList", keyGenerator = "wiselyKeyGenerator")
    public String queryBySqlCharList(Map<String, String> map) {
        /** 获取所有包含sql的list */
        JSONArray list = JSONObject.parseArray(map.get("list"));
        /** 获取参数 */
        @SuppressWarnings("unchecked")
        Map<String, String> params = JSONObject.parseObject(map.get("params"), Map.class);
        /** 单次查询的结果 */
        String sqlRes = null;
        /** 返回结果集 */
        JSONArray resArray = new JSONArray();
        /** 图表类型 */
        String type = null;
        /** 图表id */
        String id = null;
        try {
            if (!list.isEmpty()) {
                /** 遍历查询sql */
                int len = list.size();
                for (int i = 0; i < len; i++) {
                    JSONObject jo = list.getJSONObject(i);
                    /** 判断是否需要分页查询 */
                    id = jo.getString("id");
                    type = jo.getString("type");
                    if ("true".equals(jo.getString("page"))) {
                        params.put("minnumber", jo.getString("minnumber"));
                        params.put("maxnumber", jo.getString("maxnumber"));
                        if ("datagrid".equals(type)) {
                            sqlRes = charDataHandler.queryDataGridByPage(params);
                        } else if ("treegrid".equals(type)) {
                            sqlRes = charDataHandler.queryTreeGridByPage(params);
                        } else {
                            sqlRes = sqlExecuter.queryByPageSql(jo.getString("sql"), type, params);
                        }
                    } else {
                        sqlRes = sqlExecuter.queryBySql(jo.getString("sql"), type, params);
                    }
                    jo = new JSONObject();
                    jo.put("id", id);
                    jo.put("result", sqlRes);
                    jo.put("echarts-type", type);
                    resArray.add(jo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resArray.toString();
    }

    @SuppressWarnings("unchecked")
//	@Cacheable(value = "queryResidValue", keyGenerator = "wiselyKeyGenerator")
    public String queryResidValue(Map<String, String> map) {
        List<JSONArray> ary = new ArrayList<JSONArray>();
        JSONObject json = JSON.parseObject(map.remove("queryObj"));
        String dzb = json.getString("DZB");
        dzb = dzb.substring(1, dzb.length() - 1).replaceAll("\"", "");
        String params = map.remove("params");
        JSONObject jasonObject = JSON.parseObject(params);
        map.put("resids", dzb);
        map.put("startDate", jasonObject.getString("startDate"));
        map.put("endDate", jasonObject.getString("endDate"));
        String condition = XMLUtils.map2XMLCondition(map);

        SPSQLBean spsqlBean = new SPSQLBean("PKG_YDZG_DZB.PROC_QUERY_ZBID", new Object[]{condition}, new int[]{OracleTypes.CURSOR});

        ary = ydzgDataSpBeanDao.Query(spsqlBean, dzbSqlResultHandler);

        return ary.isEmpty() ? ary.toString() : ary.get(0).toString();
    }

    @SuppressWarnings("unchecked")
//	@Cacheable(value = "queryZdyzbValue", keyGenerator = "wiselyKeyGenerator")
    public String queryZdyzbValue(Map<String, String> map) {
        List<JSONArray> ary = new ArrayList<JSONArray>();
        Map<String, String> map1 = FormatterDate.getFormatterDate();
        JSONObject jsonObj = JSON.parseObject(map.remove("queryObj"));
        String dzb = jsonObj.getString("DZB");
        dzb = dzb.substring(1, dzb.length() - 1).replaceAll("\"", "");
        map.remove("params");
        map.put("resids", dzb);
        map.putAll(map1);
        String condition = XMLUtils.map2XMLCondition(map);

        SPSQLBean spsqlBean = new SPSQLBean("PKG_YDZG_DZB.PROC_QUERY_ZDYZBID", new Object[]{condition}, new int[]{OracleTypes.CURSOR});
        ary = ydzgDataSpBeanDao.Query(spsqlBean, dzbSqlResultHandler);

        return ary.get(0).toString();
    }

    @Override
    public String queryDataGridByPage(Map<String, String> map) {
        JSONObject ro = new JSONObject();
        /** 获取所有包含sql的list */
        String sql = map.get("sql");
        /** 获取参数 */
        @SuppressWarnings("unchecked")
        Map<String, String> params = JSONObject.parseObject(map.get("params"), Map.class);
        try {
            JSONObject data = sqlExecuter.queryByDatagridSql(sql, params);
            ro.put("state", "true");
            ro.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            ro.put("state", "false");
        }
        return ro.toJSONString();
    }

    @Override
    public String queryTreeGridByPage(Map<String, String> map) {
        JSONObject ro = new JSONObject();
        /** 获取所有包含sql的list */
        String sql = map.get("sql");
        /** 获取参数 */
        @SuppressWarnings("unchecked")
        Map<String, String> params = JSONObject.parseObject(map.get("params"), Map.class);
        try {
            JSONObject data = sqlExecuter.queryByTreegridSql(sql, params);
            ro.put("state", "true");
            ro.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            ro.put("state", "false");
        }
        return ro.toJSONString();
    }
}
