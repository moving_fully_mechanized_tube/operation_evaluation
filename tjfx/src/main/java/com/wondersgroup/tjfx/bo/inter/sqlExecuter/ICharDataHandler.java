package com.wondersgroup.tjfx.bo.inter.sqlExecuter;

import java.util.Map;


public interface ICharDataHandler {

    /**
     * 批量查询图表数据并格式化
     *
     * @param map
     * @return
     */
    String queryBySqlCharList(Map<String, String> map);

    /**
     * 查询resid结果集
     *
     * @param map
     * @return
     */
    String queryResidValue(Map<String, String> map);

    /**
     * 查询自定义指标结果集
     *
     * @param map
     * @return
     */
    String queryZdyzbValue(Map<String, String> map);

    /**
     * 查询datagrid分页
     *
     * @param map
     * @return
     */
    String queryDataGridByPage(Map<String, String> map);

    /**
     * 查询datagrid分页
     *
     * @param map
     * @return
     */
    String queryTreeGridByPage(Map<String, String> map);
}
