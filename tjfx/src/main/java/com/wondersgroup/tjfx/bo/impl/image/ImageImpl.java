package com.wondersgroup.tjfx.bo.impl.image;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.quick.framework.boot.remoteResult.ResultObject;
import com.quick.framework.boot.repository.nosql.mongodb.inter.IMongoFileOper;
import com.quick.framework.boot.repository.nosql.mongodb.inter.IMongoOperator;
import com.quick.framework.exception.ServiceException;
import com.quick.framework.util.LoggerUtil;
import com.wondersgroup.tjfx.bo.inter.image.IImage;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

@Service("imageImpl")
public class ImageImpl implements IImage {

	private static Logger logger = Logger.getLogger(ImageImpl.class);

	@Value(value = "${form.imagePath}")
	private String imagePath;

	@Autowired
	IMongoOperator mongoOperator;
	@Autowired
	IMongoFileOper mongoFileOper;

	@PostConstruct
	public void init() {
		File dir = new File(this.imagePath);
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdirs();
			logger.info("创建目录：" + this.imagePath);
		}
	}

	@Override
	public String queryImage() {
		try {
			JSONArray ary = mongoOperator.executeQuery("resource_image", null, new String[] { "TPMS" });
			return ary.toString();
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110101","查询失败", e);
		}
	}

	/**
	 * 增加图片
	 */
	@Override
	public String addImage(Map<String, Object> map) {
		ResultObject ro = new ResultObject();
		try {
			MultipartFile mFile = (MultipartFile) map.remove("mFile");
			JSONObject json = (JSONObject) JSON.toJSON(map);
			mongoFileOper.saveFile("resource_image", "content", mFile, json, imagePath);
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110102","保存失败", e);
		}
		return ro.toJsonStr();
	}

	@Override
	public String addImage(List<Map<String, Object>> list) {
		ResultObject ro = new ResultObject();
		try {
			for (Map<String, Object> map : list) {
				MultipartFile mFile = (MultipartFile) map.remove("mFile");
				JSONObject json = (JSONObject) JSON.toJSON(map);
				mongoFileOper.saveFile("resource_image", "content", mFile, json, imagePath);
			}
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110103","保存失败", e);
		}
		return ro.toJsonStr();
	}

	@Override
	/**
	 * 删除图片资源
	 * @param request
	 * @return
	 */
	public String deleteImage(String id) {
		ResultObject ro = new ResultObject();
		try {
			mongoFileOper.deleteFile("resource_image", id, imagePath);
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110104","删除失败", e);
		}
		return ro.toJsonStr();
	}

	/**
	 * 获取图片
	 */
	@Override
	public void retriveImg(String resid, OutputStream os) throws Exception {
		try {
			byte[] file = mongoFileOper.receiveFile("resource_image", "content", resid, imagePath);
			os.write(file);
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110105","获取图片失败", e);
		}
	}

	@Override
	public void retrivePreImg(String resid, OutputStream os, int width, int height) throws Exception {

	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String getImageByte(String resid) {
		byte[] file=null;
		String fileStr= "";
		try {
			 file = mongoFileOper.receiveFile("resource_image", "content", resid, imagePath);
			 fileStr= new String(Base64.encodeBase64(file));
		} catch (Exception e) {
			LoggerUtil.error(e.getMessage());
			throw new ServiceException("ERR-110105","获取图片失败", e);
		}
		return fileStr;
	}

}
