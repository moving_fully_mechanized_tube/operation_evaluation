package com.wondersgroup.tjfx.bo.inter.form;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.quick.framework.boot.remoteResult.ResultObject;

/**
 * @Author WEI
 * @Description
 * @Date Created in 2018/06/15 10点29分01秒
 */
public interface IFormPz {
    ResultObject saveForm(String formId, String jsonStr);

    JSONArray queryList(String key);

    JSONObject queryForm(String formId);

    ResultObject deleteForm(String formId);
}
