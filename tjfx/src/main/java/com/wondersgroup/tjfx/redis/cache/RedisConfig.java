package com.wondersgroup.tjfx.redis.cache;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonAutoDetect;  
@Configuration
@EnableCaching
@ConfigurationProperties(prefix = "common.redis")
public class RedisConfig  extends CachingConfigurerSupport{

	private static Logger logger = Logger.getLogger(RedisConfig.class);

	
	private String host;
	
	private int port;
	
	@Bean
	public KeyGenerator wiselyKeyGenerator() {
		return new KeyGenerator() {
			@Override
			public Object generate(Object target, Method method, Object... params) {
				StringBuilder sb = new StringBuilder();
				sb.append(target.getClass().getName());
				sb.append(method.getName());
				for (Object obj : params) {
					sb.append(obj.toString());
				}
				return sb.toString();
			}
		};
	}
	
	
	  	@Bean  
	    public JedisConnectionFactory redisConnectionFactory() {  
	        JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();  
	  
	        // Defaults  
	        redisConnectionFactory.setHostName(host);  
	        redisConnectionFactory.setPort(port);  
	        return redisConnectionFactory;  
	    }  
	 
	  /**
	     * RedisTemplate配置
	     * @param factory
	     * @return
	     */
	    @Bean
	    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory cf) {
	        StringRedisTemplate template = new StringRedisTemplate(cf);
	        //定义key序列化方式
	        RedisSerializer<String> redisSerializer = new StringRedisSerializer();//Long类型会出现异常信息;需要我们上面的自定义key生成策略，一般没必要
	        //定义value的序列化方式
	        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
	        ObjectMapper om = new ObjectMapper();
	        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
	        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
	        jackson2JsonRedisSerializer.setObjectMapper(om);
	        template.setKeySerializer(redisSerializer);
	        template.setValueSerializer(jackson2JsonRedisSerializer);
	        template.setHashValueSerializer(jackson2JsonRedisSerializer);
	        template.afterPropertiesSet();
	        return template;
	    }
	    
	    @Bean  
	    public CacheManager cacheManager(RedisTemplate redisTemplate) {  
	        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);  
	  
	        cacheManager.setDefaultExpiration(30); // Sets the default expire time (in seconds)  
	        return cacheManager;  
	    }


		public String getHost() {
			return host;
		}


		public void setHost(String host) {
			this.host = host;
		}


		public int getPort() {
			return port;
		}


		public void setPort(int port) {
			this.port = port;
		}

	    
	    
	    
}
