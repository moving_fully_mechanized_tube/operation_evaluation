package com.wondersgroup.tjfx.controller.form;

import com.alibaba.fastjson.JSONObject;
import com.wondersgroup.tjfx.bo.inter.form.IForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 条件选择页面配置
 *
 * @author WEI
 */
@RestController
@RequestMapping("/form")
public class FormRestController {
    @Autowired
    IForm form;

    /**
     * 查询
     *
     * @param map
     * @return
     */
    @RequestMapping("/queryForm/{type}")
    public String queryForm(@RequestParam(value = "formMetadata") String formMetadata, @PathVariable("type") String type) {
        return form.queryForm(formMetadata, type);
    }

    @RequestMapping("/queryFormXx/{type}")
    public String queryFormXx(@PathVariable("type") String type) {
        String queryFormXx = form.queryFormXx(type);
        return queryFormXx;
    }

    /**
     * 获取
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/receiveForm/{tableName}/{id}")
    public String receiveForm(@PathVariable("tableName") String tableName, @PathVariable("id") String id) {
        return form.receiveForm(tableName, id);
    }

    /**
     * 保存模板配置
     * String BDID,String json,String type
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/saveForm/{type}", method = RequestMethod.POST)
    public String saveForm(@PathVariable("type") String type, @RequestBody JSONObject jsonStr, @RequestParam(value = "formId") String formId) {
        return form.saveForm(formId, jsonStr.toString(), type);
    }

    /**
     * 删除模板配置
     *
     * @param request
     * @return
     */
    @RequestMapping("/deleteForm/{type}/{formId}")
    public String deleteForm(@PathVariable("formId") String formId, @PathVariable("type") String type) {
        return form.deleteForm(formId, type);
    }

    /**
     * 根据类型获取resource_iframe表的数据
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/receiveIframe/{type}")
    public String receiveIframe(@PathVariable("type") String type) {
        return form.receiveIframe(type);
    }

    /**
     * 根据表名获取内容
     *
     * @param tableName
     * @return
     */
    @RequestMapping(value = "/receiveFormByTableName/{tableName}")
    public String receiveFormByTableName(@PathVariable("tableName") String tableName) {
        return form.receiveForm(tableName);
    }

    /**
     * 查询mongo中已配置视图的指标代码
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/queryZbst/{type}")
    public String queryZbst(@PathVariable("type") String type) {

        return form.queryZbst(type);
    }

    /**
     * 查询页面
     *
     * @param type   表名
     * @param name   关键字
     * @param isLink 是否下钻页面
     * @return
     */
    @RequestMapping(value = "/queryFormList/{type}")
    public String queryForm(@PathVariable("type") String type, @RequestParam("name") String name, @RequestParam("isLink") String isLink) {
        return form.queryLinked(type, name, isLink);
    }

}
