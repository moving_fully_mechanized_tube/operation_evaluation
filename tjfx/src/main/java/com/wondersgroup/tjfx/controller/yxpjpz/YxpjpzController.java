package com.wondersgroup.tjfx.controller.yxpjpz;

import com.alibaba.fastjson.JSONObject;
import com.quick.framework.boot.web.wapper.QuickPrincipal;
import com.wondersgroup.tjfx.bo.inter.tjfx.IYxpjpz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 条件选择页面配置
 *
 * @author WEI
 */
@Controller
@RequestMapping("/yxpjpz")
public class YxpjpzController {

    @Autowired
    IYxpjpz yxpjpz;


    @RequestMapping("/toIndex.html")
    public String toIndex() {
        return "yxpjpz/yxpjpz_index";
    }

    @RequestMapping("/toManager.html")
    public String toManager() {
        return "yxpjpz/yxpjpz_manager";
    }

    /**
     * 查询
     *
     * @param map
     * @return
     */
    @RequestMapping("/queryYxpjpz")
    @ResponseBody
    public String queryYxpjpz(Map<String, String> map) {
        return yxpjpz.queryYxpjpz(map);
    }

    /**
     * 配置页面获取表单的内容
     *
     * @param BDID 表单ID
     * @return
     */
    @RequestMapping(value = "/receiveYxpjpz")
    @ResponseBody
    public String receiveYxpjpz(String BDID) {
        String form = yxpjpz.receiveYxpjpz(BDID);
        return form;
    }

    /**
     * 视图页面获取格式化的html
     *
     * @param BDID 表单ID
     * @return
     */
    @RequestMapping(value = "/receiveYxpjView", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String receiveYxpjView(HttpServletRequest request,String BDID) {
        QuickPrincipal principal = (QuickPrincipal)request.getUserPrincipal();
        JSONObject userInfo = principal.getUserInfo();
        String form = yxpjpz.receiveYxpjView(BDID);
        JSONObject yxpjForm = JSONObject.parseObject(form);
        String formatContent = yxpjForm.getString("formatContent");
        formatContent = formatContent.replaceAll("#userInfo#",userInfo.toString());
        return formatContent;
    }

    /**
     * 保存模板配置
     *
     * @return
     * @throws UnsupportedEncodingException
     */
    @RequestMapping("/saveYxpjpz")
    @ResponseBody
    public String saveYxpjpz(HttpServletRequest request) throws UnsupportedEncodingException {
        Map<String, String> map = getRequestMap(request);
        return yxpjpz.saveYxpjpz(map);
    }

    /**
     * 删除模板配置
     *
     * @return
     */
    @RequestMapping("/deleteYxpjpz")
    @ResponseBody
    public String deleteYxpjpz(String BDID) {
        return yxpjpz.deleteYxpjpz(BDID);
    }

    /**
     * 查询下钻页面资源
     * @param name 搜索关键字
     * @return
     */
    @RequestMapping("/queryLinked")
    @ResponseBody
    public String queryLinked(String name){
        return yxpjpz.queryFormList(name,"true");
    }

    /**
     * 查询指标页面资源，非下钻页面
     * @param name 搜索关键字
     * @return
     */
    @RequestMapping("/queryUnLinked")
    @ResponseBody
    public String queryUnLinked(String name){
        return yxpjpz.queryFormList(name,"false");
    }

    private Map<String, String> getRequestMap(HttpServletRequest request) {
        Map<String, String> json = new HashMap();
        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            String value = request.getParameter(name);
            try {
                value = URLDecoder.decode(value, "UTF-8");
                value = URLDecoder.decode(value, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            json.put(name, value);
        }
        return json;
    }
}
