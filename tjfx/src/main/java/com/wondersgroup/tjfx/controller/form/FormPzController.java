package com.wondersgroup.tjfx.controller.form;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.quick.framework.boot.remoteResult.ResultObject;
import com.wondersgroup.tjfx.bo.inter.form.IFormPz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @Author WEI
 * @Description
 * @Date Created in 2018/06/12 22点45分57秒
 */
@Controller
@RequestMapping("/formPz")
public class FormPzController {

    @Autowired
    private IFormPz formPz;

    @RequestMapping("/toManager.html")
    public String toManager() {
        return "form/form_manager";
    }

    @RequestMapping("/toIndex.html")
    public String toIndex() {
        return "form/form_index";
    }

    @RequestMapping("/index.html")
    public String index() {
        return "form/index";
    }

    /**
     * 保存模板配置
     * String BDID,String json,String type
     *
     * @return
     */
    @RequestMapping(value = "/saveForm", method = RequestMethod.POST)
    @ResponseBody
    public ResultObject saveForm(String jsonStr) {
        try {
            jsonStr = URLDecoder.decode(jsonStr, "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        JSONObject json = JSONObject.parseObject(jsonStr);
        String formId = json.getString("formId");
        return formPz.saveForm(formId, json.toString());
    }

    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
    public JSONArray queryList(String key) {
        return formPz.queryList(key);
    }

    @RequestMapping(value = "/queryForm", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject queryForm(String formId) {
        return formPz.queryForm(formId);
    }

    @RequestMapping(value = "/deleteForm", method = RequestMethod.POST)
    @ResponseBody
    public ResultObject deleteForm(String formId) {
        return formPz.deleteForm(formId);
    }
}
