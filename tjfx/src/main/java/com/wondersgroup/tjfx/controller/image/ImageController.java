package com.wondersgroup.tjfx.controller.image;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/image")
public class ImageController {
	
	/**
	 * 图片资源查询界面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "toIndex")
	public String toIndex(HttpServletRequest request) {
		return "console/image/image_index";
	}

	/**
	 * 图片资源管理界面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "togManager")
	public String togManager(HttpServletRequest request) {
		return "console/image/image_manager";
	}
	
}
